import 'package:flutter/material.dart';
class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}
class AppState extends State<App> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    var appWidget = MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('My Image Viewer'),),
          body: Text('Hello count is = $counter'),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              counter++;
              print('counter=$counter');
              setState(() {
              });
            },

          ),
        )
    );

    return appWidget;
  }
}